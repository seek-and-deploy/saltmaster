install_gw_packages:
  pkg.installed:
    - pkgs:
      - alsa-base
      - alsa-utils
      - attr
      - autossh
      - ecryptfs-utils
      - emacs-nox
      - ntp
      - python-pip
      - python-xattr
      - redis-tools
      - unzip
      - wget

remove_gw_packages:
  pkg.removed:
    - pkgs:
        - nginx-common
        - nginx-core
        - nginx-core-dbg
        - nginx-doc
        - nginx-extras
        - nginx-extras-dbg
        - nginx-full
        - nginx-full-dbg
        - nginx-light
        - nginx-light-dbg
    - require:
        - install_gw_packages

redis-pip:
  pip.installed:
    - name: redis >= 2.10.6, <= 2.11

simplejson-pip:
  pip.installed:
    - name: simplejson >= 3.13.2, <= 3.14

pyOpenSSL:
  pip.installed:
    - name: pyOpenSSL == 18.0.0
    - upgrade: True
    - require:
      - pkg: python-pip
  
