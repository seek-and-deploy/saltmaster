# -*- terraform -*-
data "template_file" "saltmaster_userdata" {
  vars {
    project_config     = "${var.project_config}"
    tag_keys           = "${join(",", keys(var.tags))}"
    tag_values         = "${join(",", values(var.tags))}"
    salt_version       = "${var.saltmaster_map["version"]}"
    hostname           = "${var.pri_dns_name}"
    pri_dns_domain     = "${replace(var.pri_dns_domain, "/.$/", "")}"
    eni_id             = "${aws_network_interface.saltmaster.id}"
    eip_alloc_id       = "${var.eip_allocation_id}"
    eni_pri_ip         = "${element(aws_network_interface.saltmaster.private_ips, 0)}"
  }
  template = "${file("${path.module}/templates/saltmaster_userdata.tpl")}"
}


# Define the Launch Configuration
resource "aws_launch_configuration" "saltmaster" {
  count                = "${var.elb_map["use_elb"] || var.asg_map["use_asg"] ? 1 : 0}"
  name_prefix          = "${var.tags["Name"]}"
  associate_public_ip_address = true
  image_id             = "${var.ec2_ami}"
  instance_type        = "${var.ec2_instance_type}"
  key_name             = "${var.aws_key_name}"
  security_groups      = [ "${split(",", var.sg_ids)}",
                           "${aws_security_group.salt_sg.id}"]
  iam_instance_profile = "${var.instance_profile}"
  user_data            = "${data.template_file.saltmaster_userdata.rendered}"

  root_block_device = {
    volume_type = "${var.ec2_map["root_volume_type"]}"
    volume_size = "${var.ec2_map["root_volume_size"]}"
    delete_on_termination = true
  }
  
  lifecycle {
    create_before_destroy = true
  }
}
