{% from 'common/map.jinja' import common with context -%}
shortGWURL:
  grains.present:
    - value: {{ common.short_hostname }}
    - force: True
