/etc/salt/minion:
  file:
    - managed
    - template: jinja
    - source: salt://salt/files/minion

salt-minion:
  pkg.installed: []
  service.running:
    - watch:
        - file: /etc/salt/minion
