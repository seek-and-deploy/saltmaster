{% set installedVersion = grains['installedVersion'] %}
{% set stagedVersion    = grains['stagedVersion'] %}
{% set runningVersion   = grains['runningVersion'] %}
{% set uninstallVersion = grains['uninstallVersion'] %}

##
# Stop old version
##
{% if grains['uninstallVersion'] %}
stop_acmegw_docker_containers:
  docker_container.stopped:
    - containers:
      - quay.io/acmesys/redis:{{ uninstallVersion }}
      - quay.io/acmesys/logtool:{{ uninstallVersion }}
      - quay.io/acmesys/s3rpc:{{ uninstallVersion }}
      - quay.io/acmesys/mdonvif:{{ uninstallVersion }}
      - quay.io/acmesys/mdsync:{{ uninstallVersion }}
      - quay.io/acmesys/mdpolicy:{{ uninstallVersion }}
      - quay.io/acmesys/wscomm:{{ uninstallVersion }}
      - quay.io/acmesys/wsproxy:{{ uninstallVersion }}
      - quay.io/acmesys/cds:{{ uninstallVersion }}
      - quay.io/acmesys/wstunnel:{{ uninstallVersion }}
      - quay.io/acmesys/vsmanager:{{ uninstallVersion }}
      - quay.io/acmesys/vss:{{ uninstallVersion }}
      - quay.io/acmesys/sas:{{ uninstallVersion }}
      - quay.io/acmesys/uis:{{ uninstallVersion }}
      - quay.io/acmesys/mda:{{ uninstallVersion }}
      - quay.io/acmesys/stunnel:{{ uninstallVersion }}
      - quay.io/acmesys/aes:{{ uninstallVersion }}
      - quay.io/acmesys/nginxssl:{{ uninstallVersion }}
    - force: True
    - require:
      - stage_acmegw_docker_containers

###
# Remove old version
##
remove_acmegw_docker_containers:
  docker_image.absent:
    - images:
      - quay.io/acmesys/redis:{{ uninstallVersion }}
      - quay.io/acmesys/logtool:{{ uninstallVersion }}
      - quay.io/acmesys/s3rpc:{{ uninstallVersion }}
      - quay.io/acmesys/mdonvif:{{ uninstallVersion }}
      - quay.io/acmesys/mdsync:{{ uninstallVersion }}
      - quay.io/acmesys/mdpolicy:{{ uninstallVersion }}
      - quay.io/acmesys/wscomm:{{ uninstallVersion }}
      - quay.io/acmesys/wsproxy:{{ uninstallVersion }}
      - quay.io/acmesys/cds:{{ uninstallVersion }}
      - quay.io/acmesys/wstunnel:{{ uninstallVersion }}
      - quay.io/acmesys/vsmanager:{{ uninstallVersion }}
      - quay.io/acmesys/vss:{{ uninstallVersion }}
      - quay.io/acmesys/sas:{{ uninstallVersion }}
      - quay.io/acmesys/uis:{{ uninstallVersion }}
      - quay.io/acmesys/mda:{{ uninstallVersion }}
      - quay.io/acmesys/stunnel:{{ uninstallVersion }}
      - quay.io/acmesys/aes:{{ uninstallVersion }}
      - quay.io/acmesys/nginxssl:{{ uninstallVersion }}
    - force: True
    - require:
      - stage_acmegw_docker_containers
{% endif %}


###
# Stage new version
##
{% if grains['stagedVersion'] %}
stage_acmegw_docker_containers:
  docker_image.present:
    - tag: {{ stagedVersion }}
    - names:
      - quay.io/acmesys/redis
      - quay.io/acmesys/logtool
      - quay.io/acmesys/s3rpc
      - quay.io/acmesys/mdonvif    
      - quay.io/acmesys/mdsync
      - quay.io/acmesys/mdpolicy
      - quay.io/acmesys/wscomm
      - quay.io/acmesys/wsproxy
      - quay.io/acmesys/cds
      - quay.io/acmesys/wstunnel
      - quay.io/acmesys/vsmanager
      - quay.io/acmesys/vss
      - quay.io/acmesys/sas
      - quay.io/acmesys/uis
      - quay.io/acmesys/mda
      - quay.io/acmesys/stunnel
      - quay.io/acmesys/aes
      - quay.io/acmesys/nginxssl
    - require:
      - sls: gateway.docker

stop_staged_acmegw_docker_containers:
  docker_container.stopped:
    - containers:
      - quay.io/acmesys/redis:{{ stagedVersion }}
      - quay.io/acmesys/logtool:{{ stagedVersion }}
      - quay.io/acmesys/s3rpc:{{ stagedVersion }}
      - quay.io/acmesys/mdonvif:{{ stagedVersion }}
      - quay.io/acmesys/mdsync:{{ stagedVersion }}
      - quay.io/acmesys/mdpolicy:{{ stagedVersion }}
      - quay.io/acmesys/wscomm:{{ stagedVersion }}
      - quay.io/acmesys/wsproxy:{{ stagedVersion }}
      - quay.io/acmesys/cds:{{ stagedVersion }}
      - quay.io/acmesys/wstunnel:{{ stagedVersion }}
      - quay.io/acmesys/vsmanager:{{ stagedVersion }}
      - quay.io/acmesys/vss:{{ stagedVersion }}
      - quay.io/acmesys/sas:{{ stagedVersion }}
      - quay.io/acmesys/uis:{{ stagedVersion }}
      - quay.io/acmesys/mda:{{ stagedVersion }}
      - quay.io/acmesys/stunnel:{{ stagedVersion }}
      - quay.io/acmesys/aes:{{ stagedVersion }}
      - quay.io/acmesys/nginxssl:{{ stagedVersion }}
    - force: True
    - require:
      - stage_acmegw_docker_containers

{% endif %}

###
# Run running version
###
{% if grains['runningVersion'] %}
pull_acmegw_docker_containers:
  docker_image.present:
    - tag: {{ runningVersion }}
    - names:
      - quay.io/acmesys/redis
      - quay.io/acmesys/logtool
      - quay.io/acmesys/s3rpc
      - quay.io/acmesys/mdonvif    
      - quay.io/acmesys/mdsync
      - quay.io/acmesys/mdpolicy
      - quay.io/acmesys/wscomm
      - quay.io/acmesys/wsproxy
      - quay.io/acmesys/cds
      - quay.io/acmesys/wstunnel
      - quay.io/acmesys/vsmanager
      - quay.io/acmesys/vss
      - quay.io/acmesys/sas
      - quay.io/acmesys/uis
      - quay.io/acmesys/mda
      - quay.io/acmesys/stunnel
      - quay.io/acmesys/aes
      - quay.io/acmesys/nginxssl
    - require:
      - sls: gateway.docker

run_acmegw_docker_containers:
  docker_container.running:
    - containers:
      - quay.io/acmesys/redis:{{ runningVersion }}
      - quay.io/acmesys/logtool:{{ runningVersion }}
      - quay.io/acmesys/s3rpc:{{ runningVersion }}
      - quay.io/acmesys/mdonvif:{{ runningVersion }}
      - quay.io/acmesys/mdsync:{{ runningVersion }}
      - quay.io/acmesys/mdpolicy:{{ runningVersion }}
      - quay.io/acmesys/wscomm:{{ runningVersion }}
      - quay.io/acmesys/wsproxy:{{ runningVersion }}
      - quay.io/acmesys/cds:{{ runningVersion }}
      - quay.io/acmesys/wstunnel:{{ runningVersion }}
      - quay.io/acmesys/vsmanager:{{ runningVersion }}
      - quay.io/acmesys/vss:{{ runningVersion }}
      - quay.io/acmesys/sas:{{ runningVersion }}
      - quay.io/acmesys/uis:{{ runningVersion }}
      - quay.io/acmesys/mda:{{ runningVersion }}
      - quay.io/acmesys/stunnel:{{ runningVersion }}
      - quay.io/acmesys/aes:{{ runningVersion }}
      - quay.io/acmesys/nginxssl:{{ runningVersion }}
    - force: True
    - require:
      - pull_acmegw_docker_containers
{% endif %}
