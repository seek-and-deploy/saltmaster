# -*- terraform -*-
resource "aws_route53_record" "saltmaster_pub_cname_record_elb" {
  count   = "${var.elb_map["use_elb"] ? 1 : 0}"
  depends_on = [ "aws_elb.saltmaster-ext" ]
  zone_id = "${var.public_dns_domain_id}"
  name    = "${var.pub_dns_name}"
  type    = "A"
  alias {
    name    = "${aws_elb.saltmaster-ext.dns_name}"
    zone_id = "${aws_elb.saltmaster-ext.zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "saltmaster_pri_cname_record_elb_asg" {
  count   = "${var.elb_map["use_elb"] ? 1 : 0}"
  zone_id = "${var.private_dns_domain_id}"
  name    = "${var.pri_dns_name}"
  type    = "A"

  alias {
    name = "${aws_elb.saltmaster-int.dns_name}"
    zone_id = "${aws_elb.saltmaster-int.zone_id}"
    evaluate_target_health = false
  }
}

output "saltmaster_public_cname_elb_asg" {
  value = "${ aws_route53_record.saltmaster_pub_cname_record_elb.*.fqdn }"
}

output "canonical_elb_ext_dns_name"  {
  value = "${aws_elb.saltmaster-ext.*.dns_name}"
}

output "elb_ext_hosted_zone"  {
  value = "${aws_elb.saltmaster-ext.*.zone_id}"
}

output "canonical_elb_int_dns_name"  {
  value = "${aws_elb.saltmaster-int.*.dns_name}"
}
output "elb_int_hosted_zone"  {
  value = "${aws_elb.saltmaster-int.*.zone_id}"
}
