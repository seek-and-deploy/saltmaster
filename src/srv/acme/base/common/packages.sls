common_packages:
  pkg.installed:
    - pkgs:
        - apt-transport-https
        - curl
        - emacs24-nox
        - openssh-client
        - openssh-server
        - python
        - python-pip
        - python2.7-dev
        - python3
        - python3-pip
        - python-software-properties

python-pip:
  pkg.installed:
    - name: python-pip
    - reload_modules: True

pip-upgrade:
  pip.installed:
    - name: pip >= 9.0.3, <= 9.1
    - upgrade: True
    - require:
      - pkg: python-pip
