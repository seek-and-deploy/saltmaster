log_new_minion:
  local.cmd.run:
    - name: log new minion
    - tgt: 'saltmaster*'
    - arg:
      - 'echo "[{{ data['id'] }}][minion started] A new Minion has (re)born on $(date). Say hello to him ({{ tag }})" >> /tmp/salt.reactor.log'

initialize_state:
  local.state.apply:
    - tgt: {{ data['id'] }}