# -*- terraform -*-
# Create an ELB load balancer to sit in front
resource "aws_elb" "saltmaster-int" {
  count                     = "${var.elb_map["use_elb"] ? 1 : 0}"
  name                      = "${var.tags["system_type"]}-int-${var.env_name}"
  internal                  = true
  cross_zone_load_balancing = true
  security_groups           = [ "${aws_security_group.salt_sg.id}",
                                "${aws_security_group.saltmaster_elb_int.id}"
                              ]
  subnets = ["${split(",", var.pri_subnet_ids)}"]

  listener {
    lb_port               = 443
    lb_protocol           = "tcp"
    instance_port         = 8443
    instance_protocol     = "tcp"
  }

  health_check {
    healthy_threshold     = "${var.elb_map["elb_healthy_threshold"]}" 
    unhealthy_threshold   = "${var.elb_map["elb_unhealthy_threshold"]}"
    timeout               = "${var.elb_map["elb_timeout"]}"
    interval              = "${var.elb_map["elb_interval"]}"
    target                = "${var.elb_map["elb_health-check"]}"
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = "${merge(var.tags,
                  map("Name", "${var.tags["system_type"]}-int-${var.env_name}"),
                  map("env",  "${var.env_name}"))}"
  
}


output "saltmaster_elb_int_id" {
  value = "${aws_elb.saltmaster-int.*.id}"
}
output "saltmaster_elb_int_name" {
  value = "${aws_elb.saltmaster-int.*.name}"
}
output "saltmaster_elb_int_dns_name" {
  value = "${aws_elb.saltmaster-int.*.dns_name}"
}
output "saltmaster_elb_int_instances" {
  value = "${aws_elb.saltmaster-int.*.instances}"
}
output "saltmaster_elb_int_source_security_group" {
  value = "${aws_elb.saltmaster-int.*.source_security_group}"
}
output "saltmaster_elb_int_source_security_group_id" {
  value = "${aws_elb.saltmaster-int.*.source_security_group_id}"
}
output "saltmaster_elb_int_zone_id" {
  value = "${aws_elb.saltmaster-int.*.zone_id}"
}
