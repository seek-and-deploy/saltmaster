# Modify rsyslog on systems to:
# if log message has [acmesys in it, save to /var/log/acmesys.log
# set up logrotate for /var/log/acmesys.log
#   fix rsyslog global variables to user syslog:syslog so logrotate works
# restart rsyslog if any changes were made
rsyslog:
  pkg.installed: []
  service.running:
    - name: syslog
    - enable: True
    - watch:
        - file: logrotate_conf
        - file: rsyslog_conf

logrotate_conf:
  file.managed:
    - name: /etc/rsyslog.d/60-acmesys.conf
    - source: 'salt://common/files/gw-rsyslog-acmesys.conf'
    - user: root
    - group: root
    - mode: 644
    - require:
        - pkg: rsyslog

rsyslog_conf:
  file.managed:
    - name: /etc/rsyslog.conf
    - source: 'salt://common/files/gw-rsyslog-acmesys.conf'
    - user: root
    - group: root
    - mode: 660
    - require:
        - pkg: rsyslog
