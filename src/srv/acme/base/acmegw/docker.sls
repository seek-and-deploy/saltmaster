include:
  - acmesvr.docker

docker_svc:
  service.running:
    - name: docker
    - enable: True
    - watch:
        - file: docker_conf

docker_conf:
  file.managed:
    - name: /etc/systemd/system/docker.service.d/env.conf
    - source: "salt://gateway/files/docker.conf"
    - makedirs: true
    - require:
        - sls: acmesvr.docker

docker-pip-pkg:
  pip.installed:
    - name: docker
    - reload_modules: True
    - require:
      - sls: gateway.packages

/home/gateway/.docker/config.json:
  file.managed:
   - source: 'salt://gateway/files/quay_creds.json'
   - makedirs: True
   - user: gateway
   - group: gateway
   - mode: 600
   - force: true

/root/.docker/config.json:
  file.managed:
   - source: 'salt://gateway/files/quay_creds.json'
   - makedirs: True
   - user: root
   - group: root
   - mode: 600
   - force: true

/home/factory/.docker/config.json:
  file.managed:
   - source: 'salt://gateway/files/quay_creds.json'
   - makedirs: True
   - user: factory
   - group: factory
   - mode: 600
   - force: true

{% set docker_tag = grains['versiontag'] %}
pull_docker_containers:
  docker_image.present:
    - name: quay.io/acmesys/deploy
    - tag: {{ docker_tag }}
    - unless:
      - ls /data/acmesys
    - require:
      - file: /home/gateway/.docker/config.json
      - file: /root/.docker/config.json
      - file: /home/factory/.docker/config.json


##
# This entire section can be removed if we roll a deb
##
run_docker_containers:
  docker_container.run:
    - name: deploy.acmesys
    - image: quay.io/acmesys/deploy:{{ grains['versiontag'] }}
    - binds: /:/hostmap
    - environment:
      - INSTALL_ONLY: y
    - unless:
      - ls /data/acmesys/syncGatewayIP.sh
    - require:
        - pull_docker_containers

remove_docker_containers:
  docker_container.absent:
    - name: deploy.acmesys
    - onlyif:
      - ls /data/acmesys/syncGatewayIP.sh
    - require:
      - pull_docker_containers
  
remove_docker_images:
  docker_image.absent:
    - name: quay.io/acmesys/deploy:{{ grains['versiontag'] }}
    - force: True
    - require:
      - remove_docker_containers



