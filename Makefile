PKGNAME=acme-saltmaster
MAJOR_VERSION=1
MINOR_VERSION=0
PKGDIR = $(BUILD_DIR)
REPO_DIR=/tmp/repos
DISTRIBUTIONS=conf/distributions
BUILD_VERS=$(MAJOR_VERSION).$(MINOR_VERSION)$(VERSION)
DEBVERSION=$(shell echo $(BUILD_VERS) | sed 's/[_-]//g')
BUILD_DIR=$(PKGNAME)-$(DEBVERSION)

clean-deb:
	rm -f *.deb *.dsc *.tar.xz *.changes *~

clean-repo:
	rm -rf /tmp/repos

clean-build:
	rm -rf $(PKGNAME)*

clean-all: clean-deb clean-repo clean-build

build: clean-deb clean-build
	echo "Building with version: $(DEBVERSION)"
	echo "BUILDDIR: $(BUILD_DIR)"
	mkdir -p ${BUILD_DIR}
	rsync -ra $(PWD)/src/* $(BUILD_DIR)/
	rsync -ra $(PWD)/debian $(BUILD_DIR)/
	sed -i "s/@VERSION@/$(DEBVERSION)/g" $(BUILD_DIR)/debian/changelog
	cat $(BUILD_DIR)/debian/changelog
	cd $(BUILD_DIR) && dpkg-buildpackage -us -uc -v$(DEBVERSION)

repo: clean-repo
	rsync -rav $(PWD)/repos /tmp/
	reprepro -b $(REPO_DIR) includedeb stable *.deb
	reprepro -b $(REPO_DIR) createsymlinks stretch
