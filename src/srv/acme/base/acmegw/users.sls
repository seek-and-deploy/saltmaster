gateway:
  user.present:
    - fullname: Gateway User
    - shell: /bin/bash
    - home: /home/gateway
    - password: $1$kZzSZOqD$FmfHW2WuQVc08kNciQD0x0
    - groups:
      - docker
    - optional_groups:
      - wheel
      - lxd
      - adm
      - cdrom
      - sudo
      - dip
      - plugdev
      - lpadmin
      - sambashare

# add the factory audio test user
factory:
  user.present:
    - fullname: Factory User
    - shell: /data/acmesys/audiotest.sh
    - home: /home/factory
    - password: $6$ymEwJjoD$HxrFzIyk4yErFLOCYYTgAcrN2bylqgJGIiFkc5I1cKMeUX9IxwsXjduPceJFk.XUaK6qr4B9TIppgQJd7TJ9r/
    - groups:
      - docker
    - optional_groups:
      - adm
      - cdrom
      - dip
      - lpadmin
      - sambashare
  file.managed:
    - name: /data/acmesys/audiotest.sh
    - source: salt://gateway/files/audiotest.sh
    - mode: 755
    - replace: True
    - makedirs: True

