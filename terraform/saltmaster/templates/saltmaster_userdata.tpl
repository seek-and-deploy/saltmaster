#!/bin/bash

# NOTE: The interpreted form of this template can be found on the ec2
# instance at: /var/lib/cloud/instance/scripts/part-001
#
# Global vars
md_server='http://169.254.169.254'
meta_data="$md_server/latest/meta-data"
instance_id=$(curl -s $meta_data/instance-id)
region=$(curl -s $meta_data/placement/availability-zone | sed 's/.$//g')
priv_ip=$(curl  -s $meta_data/local-ipv4)
eni_pri_ip=${eni_pri_ip}
export AWS_DEFAULT_REGION="$region"

function main {
    update_instance
    update_hostname
    if [ -n "${eni_id}" -a "${eni_id}" != "" ]
    then
    	attach_eip
    fi
    tag_instance
    install_salt
}

function update_instance {
    log "Updating Ubuntu packages"
    apt-get update
    apt-get upgrade -y
    apt-get install -y awscli python-boto
    log "Updating instance pip packages"
    /usr/local/bin/pip install --upgrade pip
    /usr/local/bin/pip install botocore awscli boto
}

function update_hostname {
    old_name=$( hostname )
    log "Updating hostname to: ${hostname}"
    hostname ${hostname}
    log "Setting /etc/hostname file with: ${hostname}"
    hostname > /etc/hostname

    log "Updating /etc/hosts with ${hostname}"
    # Note - since we're setting the private IP for the ENI in
    # terraform, we know what it it and can therefore have the
    # user-data stuff it into /etc/hosts so salt can extract it later
    # to configure the OS-level interface.
    echo "127.0.0.1 localhost" > /etc/hosts
    echo "$priv_ip ${hostname}.${pri_dns_domain} ${hostname}" >> /etc/hosts
    echo "$eni_pri_ip ${hostname}-eth1.${pri_dns_domain} ${hostname}-eth1" >> /etc/hosts

    new_name=$( hostname )
    log "Changed '$old_name' to '$new_name'"

}

# Tag instances on Launch from ASG, since terraform is stupid.
#
function tag_instance {
    log "Tagging instance with keys/values: ${tag_keys}/${tag_values}"
    IFS=',' read -r -a keys <<< "${tag_keys}"
    IFS=',' read -r -a vals <<< "${tag_values}"
    cli_cmd="aws ec2 create-tags --region $region --resources $instance_id "
    for i in "$${!keys[@]}"
    do
        $cli_cmd --tags "Key=$${keys[$i]},Value=$${vals[$i]}"
    done
}

function install_salt {
    ec2_desc="ec2 describe-instances"
    ec2_inst="--instance-id=$instance_id"
    query="--query "Reservations[*].Instances[*].Tags[?Key=='env_name'].Value" --output text"
    cli_cmd="aws --region $region $ec2_desc $ec2_inst "
    cli_cmd="$cli_cmd $ec2_inst $query"
    env_name=$( $cli_cmd )

    log "Installing salt master == ${salt_version}"

    SALT_DIR=$(echo ${salt_version} | cut -f1,2 -d.)
    REPO="http://repo.saltstack.com/apt/ubuntu/18.04/amd64/$SALT_DIR"
    DEB="deb $REPO bionic main"

    wget -O - $REPO/SALTSTACK-GPG-KEY.pub | sudo apt-key add -

    echo $DEB > /etc/apt/sources.list.d/saltstack.list
    apt-get update
    for i in master minion api
    do
	apt-get install -y salt-$i="${salt_version}"
    done
    apt-get install -y python-pip nginx pollinate virt-what
    pip install --upgrade cherrypy
    pip install --upgrade pip

    log "Shutting down Salt to replace default config"
    systemctl stop salt-master
    systemctl stop salt-minion

    log "Removing default configuration files:"
    for file in master minion minion_id 
    do
	echo "Removing: /etc/salt/$file"
	rm -f /etc/salt/$file
    done
    log "Removing default PEM files:"
    for file in $(find /etc/salt -name \*.pem)
    do
	echo "Removing PEM File: $file"
	rm -f $file
    done

    # log "Copying acme-saltmaster config from S3"
    # aws s3 cp s3://${project_config}/artifacts/acme-saltmaster_1.0_amd64.deb /tmp

    log "Adding acme salt repo to apt sources"

    # This should probably NOT be hard-coded. But since I have limited
    # time left here...
    apt-add-repository "deb http://${var.account_id}-${var.project}-data.s3-website-us-east-1.amazonaws.com/artifacts/repo/ stable [trusted=yes] main"

    # We should only need one or the other of these.
    # Really, we should be passing in a variable indicating which repo to use.
    apt-add-repository "deb http://acme-debianrepo.s3-website-us-east-1.amazonaws.com/ stable [trusted=yes] main"
    cat /etc/apt/sources.list
    apt-get update

    echo "Installing acme-saltmaster"
    PKGNAME=acme-saltmaster
    apt-cache search $PKGNAME
    apt-get install -y --allow-unauthenticated $PKGNAME

    echo "Running salt-call"
    salt-call -l debug --local -c /etc/acme/salt state.apply
}

function attach_eip() {

    log "Found eni_id. Configuring ENI/EIP with static EIP."
    ec2cmd="/usr/bin/aws ec2"
    eni="--network-interface-id ${eni_id}"
    instance="--instance-id $instance_id"
    eip="--allocation-id ${eip_alloc_id}"

    log "Attaching ENI to ${eni_id}"
    $ec2cmd attach-network-interface $eni $instance --device-index 1


    log "Associating EIP ($eip_alloc_id) with ENI (${eni_id})"
    $ec2cmd associate-address $eip $eni
    # disable source destination check
    log "Disabling source destination check on ENI"
    $ec2cmd modify-network-interface-attribute $eni --no-source-dest-check

    retcode=$?
    if [ "$retcode" -eq 0 ]
    then
	log "eni attachment successful"
    else
	log "eni attachment failed"
	exit 1;
    fi

    log "Sleeping 1 minute to allow ENI to settle"

    sleep 60
    log "ENI/EIP setup complete"

    

}

function log () {
  echo "$(date +"%b %e %T") $@"
  logger -- $(basename $0)" - $@"
  echo $(basename $0)" - $@"
}


main
