base:
  '* and not mock*':
    - common
  'saltmaster-.*':
    - match: pcre
    - salt.master
  'saltminion-.*':
    - match: pcre
    - salt.minion
  'acmesvrgw-.*':
    - match: pcre
    - gateway
  'acmesvr-.*':
    - match: pcre
    - salt.minion
    - acmesvr
  'mockminion-*':
    - match: pcre
    - mock
  'kops-*':
    - match: pcre
    - kops
