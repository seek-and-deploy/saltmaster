######################################################################
# Allow HTTP/HTTPS inbound from allowed CIDR blocks
resource "aws_security_group" "salt_sg" {
  name        = "${var.tags["system_type"]}_${var.env_name}_salt_sg"
  description = "Allow SALT traffic inbound"
  vpc_id      = "${var.vpc_id}"
  tags = "${merge(var.tags, 
                  map("Name", "${var.tags["system_type"]} ${var.env_name} Allow SALT traffic SG inbound"),
                  map("env",  "${var.env_name}"))}"

  ######################################################################
  # Inbound Traffic
  ##   
  
  # Allow ALL traffic from this SG
  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  # Salt
  ingress {
    from_port   = 4505
    to_port     = 4505
    protocol    = "tcp"
    # do not confuse local.allowed_cidr_blocks with var.allowed_cidr_blocks
    cidr_blocks = [ "0.0.0.0/0" ]
  }  

  ingress {
    from_port   = 4506
    to_port     = 4506
    protocol    = "tcp"
    # do not confuse local.allowed_cidr_blocks with var.allowed_cidr_blocks
    cidr_blocks = [ "0.0.0.0/0" ]
  }

  # HTTP/S
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    # do not confuse local.allowed_cidr_blocks with var.allowed_cidr_blocks
    cidr_blocks = [ "${ local.allowed_cidr_blocks }" ]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    # do not confuse local.allowed_cidr_blocks with var.allowed_cidr_blocks
    cidr_blocks = [ "${ local.allowed_cidr_blocks }" ]
  }
  
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    # do not confuse local.allowed_cidr_blocks with var.allowed_cidr_blocks
    cidr_blocks = [ "${ local.allowed_cidr_blocks }" ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "saltmaster_sg_id" {
  value = "${aws_security_group.salt_sg.id}"
}

# Security groups for Public/External ELB
##
resource "aws_security_group" "saltmaster_elb_ext" {
  count       = "${var.elb_map["use_elb"] ? 1 : 0}"
  name        = "${var.tags["system_type"]}-ext-${var.env_name}"
  description = "Security group for Saltmaster External ELB"
  vpc_id      = "${var.vpc_id}"
  tags = "${merge(var.tags,
                  map("Name", "${var.tags["system_type"]}-${var.tags["owner"]}-${var.env_name} Allow HTTPS traffic inbound from internet"),
                  map("env",  "${var.env_name}"))}"

  ingress {
    from_port	= 443
    to_port   	= 443 
    protocol 	= "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }

  # this is for outbound internet traffic
  egress {
    from_port 	= 0
    to_port	= 0
    protocol 	= "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "saltmaster_elb_ext_sg_id" {
  value = "${aws_security_group.saltmaster_elb_ext.*.id}"
}

# Security groups for Private/Internal ELB
##
resource "aws_security_group" "saltmaster_elb_int" {
  count       = "${var.elb_map["use_elb"] ? 1 : 0}"
  name        = "${var.tags["system_type"]}-int-${var.env_name}"
  description = "Security group for Saltmaster Internal ELB"
  vpc_id      = "${var.vpc_id}"
  tags = "${merge(var.tags,
                  map("Name", "${var.tags["system_type"]}-${var.tags["owner"]}-${var.env_name} Allow HTTPS traffic inbound from private subnets"),
                  map("env",  "${var.env_name}"))}"

  ingress {
    from_port	= 443
    to_port   	= 443 
    protocol 	= "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }

  # this is for outbound internet traffic
  egress {
    from_port 	= 0
    to_port	= 0
    protocol 	= "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "saltmaster_elb_int_sg_id" {
  value = "${aws_security_group.saltmaster_elb_int.*.id}"
}
