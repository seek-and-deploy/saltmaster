#!/bin/sh -x
DEPLOYER=/builds/terraform-deployer/bin/deployer
GEN_CONFIG=/builds/terraform-deployer/bin/gen_config

CORE_TEMPLATE="/builds/core-tf/config/acme_core_template.json"
SALT_TEMPLATE="/builds/acmecloud/salt/saltmaster/terraform/config/deploy_salt-master.json"

JOB_ID=$(echo "${CI_PIPELINE_ID}" | cut -c4-)


$GEN_CONFIG -c $SALT_TEMPLATE --var '{ "environment": { "name": "dev'${GITLAB_USER_LOGIN}P${JOB_ID}'", "version": "GET_NEXT" } }' -o /tmp/saltmaster.json
$DEPLOYER destroy -v ${SHARED_ENV_CONFIG}

$GEN_CONFIG -c $CORE_TEMPLATE --var '{ "environment": { "name": "dev'${GITLAB_USER_LOGIN}P${JOB_ID}'" } }' -o /tmp/core-tf.json
$DEPLOYER destroy -v ${SM_ENV_CONFIG}
