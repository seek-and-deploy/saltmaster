# Create our data folder
acmesys folder:
  file.directory:
    - name: /data/acmesys
    - user: gateway
    - group: gateway
    - makedirs: true
