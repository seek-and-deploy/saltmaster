install_acmesvr_packages:
  pkg.installed:
    - pkgs:
        - attr
        - redis-tools
        - python3-redis
        - python-redis
        - python3-openssl
        - python-openssl
        - python-requests
        - python-docker
        - python3-docker
        - python3-simplejson
        - python-simplejson
        - python-parsedatetime
        - python-pip

remove_acmesvr_packages:
  pkg.removed:
    - pkgs:
        - nginx-common
        - nginx-core
        - nginx-core-dbg
        - nginx-doc
        - nginx-extras
        - nginx-extras-dbg
        - nginx-full
        - nginx-full-dbg
        - nginx-light
        - nginx-light-dbg
    - require:
        - install_acmesvr_packages

nginx-common:
  service.dead:
    - name: nginx-common
    - enable: False
    - require:
        - remove_acmesvr_packages

