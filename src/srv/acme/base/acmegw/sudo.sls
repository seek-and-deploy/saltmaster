sudo:
  pkg.installed: []
  file.managed:
    - name: /etc/sudoers
    - source: "salt://gateway/files/sudoers"
    - require:
        - pkg: sudo
      
