/data:
  file.directory:
    - user: ubuntu
    - group: ubuntu
    - dir_mode: 775
    - file_mode: 664

/dev/xvdb:
  blockdev.formatted:
    - name: /dev/xvdb 
    - fs_type: ext4
  mount.mounted:
    - name: /data
    - device: /dev/xvdb
    - fstype: ext4
    - mkmnt: True
    - opts:
      - defaults
      - discard
  require:
    - file: /data
