# -*- terrarform -*-
provider "aws" {
  region                = "us-east-1"
  profile               = "${var.aws_profile}"
}

# Define remote state backend as S3
terraform {
  backend "s3" {}
}

data "terraform_remote_state" "sharedenv" {
  backend = "s3"
  config {
    region = "${var.aws_region}"
    bucket = "${var.tf_state_bucket}"
    key    = "${lookup(var.environment, "name")}.tfstate"
  }
}

data "aws_ami" "default" {
  most_recent      = true
  executable_users = ["self"]
  owners	   = ["self"]
  filter {
    name   = "name"
    values = [ "pll-ubuntu-test-ami" ]
  }
}

locals {
  version = "${lookup(var.environment, "version")}"
  pub_dns_name = "${var.tags["system_type"]}-public-${local.version}"
  pri_dns_name = "${var.tags["system_type"]}-private-${local.version}"
}

module "saltmaster" {
  source                = "./saltmaster"
  aws_profile           = "${var.aws_profile}"
  aws_key_name          = "${var.aws_key_name}"
  aws_account_id        = "${var.account_id}"
  ec2_ami               = "${data.aws_ami.default.image_id}"
  ec2_instance_type     = "${var.ec2_instance_type}"
  tags                  = "${var.tags}"
  env_name              = "${var.env_name}"
  environment           = "${var.environment}"
  tmpdir                = "${var.tmpdir}"
  project_config        = "${var.project_config}"
  vpc_id                = "${data.terraform_remote_state.sharedenv.vpc_id}"
  allowed_cidr_blocks   = "${data.terraform_remote_state.sharedenv.allowed_cidr_blocks}"
  sg_ids                = "${data.terraform_remote_state.sharedenv.sg_ids}"
  pub_subnet_ids        = "${data.terraform_remote_state.sharedenv.pub_subnet_ids}"
  pri_subnet_ids        = "${data.terraform_remote_state.sharedenv.pri_subnet_ids}"
  pub_subnet_cidrs      = "${data.terraform_remote_state.sharedenv.pub_subnet_cidrs}"
  pri_subnet_cidrs      = "${data.terraform_remote_state.sharedenv.pri_subnet_cidrs}"
  public_dns_domain_id  = "${data.terraform_remote_state.sharedenv.public_dns_domain_id}"
  pub_dns_name          = "${ local.pub_dns_name }"
  pub_dns_domain        = "${ data.terraform_remote_state.sharedenv.public_dns_domain_name }"
  private_dns_domain_id = "${data.terraform_remote_state.sharedenv.private_dns_domain_id}"
  pri_dns_name          = "${ local.pri_dns_name }"
  pri_dns_domain        = "${data.terraform_remote_state.sharedenv.private_dns_domain_name}"
  instance_profile      = "${data.terraform_remote_state.sharedenv.basic_ec2_client_instance_profile_name}"
  instance_role_id      = "${data.terraform_remote_state.sharedenv.basic_ec2_client_role_id}"
  
  saltmaster_map        = "${var.saltmaster_map}"
  ec2_map               = "${var.ec2_map}"
  eip_allocation_id     = "${var.eip_allocation_id}"
  elb_map               = "${var.elb_map}"
  asg_map               = "${var.asg_map}"
}

output "salt_version" {
  value = "${var.saltmaster_map["version"]}"
}

output "saltmaster_id" {
   value = "${module.saltmaster.saltmaster_id}"
}

output "saltmaster_private_ip" {
   value = "${module.saltmaster.saltmaster_private_ip}"
}

output "saltmaster_private_cname_single" {
  value = "${module.saltmaster.saltmaster_private_cname_single}"
}

output "saltmaster_public_cname" {
  value = "${local.pub_dns_name}.${data.terraform_remote_state.sharedenv.public_dns_domain_name}"
}

output "saltmaster_sg_id" {
  value = "${module.saltmaster.saltmaster_sg_id}"
}
