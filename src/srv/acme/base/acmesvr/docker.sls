docker_config:
  file:
    - name: /etc/systemd/system/docker.service.d/docker.service
    - managed
    - makedirs: True
    - template: jinja
    - source: salt://acmesvr/files/docker.service.template

docker-repo:
  pkgrepo.managed:
    - humanname: Docker
    - enabled: True
    - name: deb https://download.docker.com/linux/ubuntu/ xenial stable
    - dist: xenial
    - architectures: amd64
    - file: /etc/apt/sources.list.d/docker.list
    - key_url: https://download.docker.com/linux/ubuntu/gpg
    - refresh: True
    - require:
        - file: docker_config

docker-ce:
  pkg.installed: []
  service.running:
    - name: docker
    - enable: True
    - require:
        - docker-repo
        - sls: acmesvr.packages
    - watch:
        - file: docker_config

docker-group:
   group.present:
    - system: True
    - addusers:
      - ubuntu

remove_docker-py:
  pip.removed:
    - name: docker-py
    - require:
      - pkg: python-pip
