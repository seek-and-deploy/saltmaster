aws_packages:
  pkg.removed:
    - name: awscli
  pip.installed:
    - pkgs:
        - awscli
    - force_reinstall: True
    - ignore_installed: True
    - bin_env: /usr/bin/pip3
      - require:
        - sls: common.packages
