kubernetes_repo:
  pkgrepo.managed:
    - humanname: Kubernetes
    - enabled: True
    - name: deb https://apt.kubernetes.io/ kubernetes-xenial main
    - dist: kubernetes-xenial
    - architectures: amd64      
    - file: /etc/apt/sources.list.d/kubernetes.list
    - key_url: https://packages.cloud.google.com/apt/doc/apt-key.gpg
    - refresh: True

install_kubernetes_packages:
  pkg.installed:
    - pkgs:
        - kubectl
    - require:
        - kubernetes_repo
  file.managed:
    - name: /usr/local/bin/kops
    - mode: 755
    - source: https://github.com/kubernetes/kops/releases/download/1.10.0/kops-linux-amd64
    - source_hash: https://github.com/kubernetes/kops/releases/download/1.10.0/kops-linux-amd64-sha1
      
download_helm:
  archive.extracted:
    - name: /usr/local/src
    - source: https://storage.googleapis.com/kubernetes-helm/helm-v2.13.1-linux-amd64.tar.gz
    - source_hash: https://storage.googleapis.com/kubernetes-helm/helm-v2.13.1-linux-amd64.tar.gz.sha256
    
install_helm:
  file.copy:
    - name: /usr/local/bin/helm
    - source: /usr/local/src/linux-amd64/helm
    - mode: 755
    - force: True
    - makedirs: True
    - require:
        - download_helm

install_tiller:
  file.copy:
    - name: /usr/local/bin/tiller
    - source: /usr/local/src/linux-amd64/tiller
    - mode: 755
    - force: True
    - makedirs: True
    - require:
        - download_helm
        
