include:
  - salt.minion

/etc/salt/master:
  file:
    - managed
    - source: salt://salt/files/master

/etc/salt/autosign.conf:
  file:
    - managed
    - template: jinja
    - source: salt://salt/files/autosign.conf

/srv/pillar/aws.sls:
  file:
    - managed
    - template: jinja
    - source: salt://salt/files/aws.sls.template

/srv/pillar/top.sls:
  file:
    - managed
    - source: salt://salt/files/pillar_top.sls.template

salt-master:
  pkg.installed: []
  service.running:
    - watch:
        - file: /etc/salt/master
        - file: /etc/salt/autosign.conf

# The ENI private IP is set via terraform and stuffed in /etc/hosts.
# Grab that and use it to configure the interface.
{% set eth1_ip = salt['cmd.shell']('awk "/$(hostname)-eth1/ {print \$1}" /etc/hosts') %}

# Need to get our default route because 17.7 doesn't have the 'ip4_gw' grain
# When we move to 18, remove these lines and fix this in the template
{% set default_route = salt['cmd.shell']('ip route | awk "/default/ {print \$3}"') %}

/etc/network/interfaces.d/eth1.cfg:
  file:
    - managed
    - create: True
    - template: jinja
    - source: salt://salt/files/eth1_cfg.template
    - defaults:
        eth1_ip: {{ eth1_ip }}
        default_route: {{ default_route }}

ifup eth1:
  cmd.run:
    - name: ifup eth1
    - cwd: /
    - onchanges:
        - /etc/network/interfaces.d/eth1.cfg
