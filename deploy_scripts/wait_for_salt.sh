#!/bin/sh -x

echo "Waiting for Saltmaster to be ready to answer requests"

PORT4505=0
PORT4506=0
TIMEOUT=600
counter=0
while [ ${PORT4505} -eq 0 -o ${PORT4506} -eq 0 ]
do
    echo "Waiting for Salt Master at ${SM_HOST}"
    if [ $counter -le $TIMEOUT ];
    then
	sleep 5;
	counter=$((counter + 5));
    else
	# If we reach this point we've failed
	echo "ERROR: Failed Connection to port 4505/4506 on ${SM_HOST}"
	echo "Connection attempt timeout. Exiting";
	exit 1;
    fi;
    PORT4505=$(nc -v -z ${SM_HOST} 4505 2>&1 | grep -c open)
    PORT4506=$(nc -v -z ${SM_HOST} 4506 2>&1 | grep -c open)
done

echo "SUCCESS: connection on ports 4505/4506 at ${SM_HOST}"


exit 0
