#!/bin/sh -x
DEPLOYER=deployer
GEN_CONFIG=gen_config

CORE_TEMPLATE="/builds/core-tf/config/acme_core_template.json"
SALT_TEMPLATE="/builds/acmecloud/salt/saltmaster/terraform/config/deploy_salt-master.json"

JOB_ID=$(echo "${CI_PIPELINE_ID}" | cut -c4-)

$GEN_CONFIG -c $CORE_TEMPLATE --var '{ "environment": { "name": "dev'${GITLAB_USER_LOGIN}P${JOB_ID}'" } }' -o ${SHARED_ENV_CONFIG}
$DEPLOYER create -v ${SHARED_ENV_CONFIG} --bootstrap --skip-download --debug

$GEN_CONFIG -c $SALT_TEMPLATE --var '{ "environment": { "name": "dev'${GITLAB_USER_LOGIN}P${JOB_ID}'", "version": "GET_NEXT" } }' -o ${SM_ENV_CONFIG}
$DEPLOYER create -v ${SM_ENV_CONFIG} --bootstrap --skip-download --debug
