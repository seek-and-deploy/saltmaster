######################################################################
# Networking access
data "template_file" "iam_ec2_network_access_role_policy_template" {
  template = "${file("${path.module}/templates/iam_ec2_network_access_role_policy.tpl")}"
}

resource "aws_iam_role_policy" "ec2_network_access" {
    name   = "${var.env_name}-${var.tags["owner"]}_ec2_network_access_role_policy"
    role   = "${var.instance_role_id}"
    policy = "${data.template_file.iam_ec2_network_access_role_policy_template.rendered}"
}
