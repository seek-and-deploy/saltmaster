{% from 'common/map.jinja' import common with context  -%}

{% set account_id    = grains['ec2']['account_id']      %}
{% set project       = grains['ec2_tags']['project']    %}
{% set bucket        = account_id ~ "-" ~ project       %}
{% set pkg           = 'acmesys_0.1_amd64.deb'          %}
{% set lv_version    = grains['acmesvr_version']       %}
{% set server_name   = common.public_fqdn               %}
{% set acme_log_dir = '/var/log/acmesvr'              %}
{% set setup_log     = '/var/log/acmesvr/setup.log'    %}

{{ acme_log_dir }}:
  file.directory:
    - name: {{ acme_log_dir }}
    - mode: 775
    - user: ubuntu
    - group: ubuntu

acmesys:
  file.managed:
      - name: /tmp/{{ pkg }}
      - source: s3://{{bucket}}-data/artifacts/{{ pkg }}
      - keep_source: False
      - skip_verify: True
  pkg:
      - installed
      - sources:
          - acmesys: /tmp/{{ pkg }}
      - require:
          - file: /tmp/{{ pkg }}
          - sls: acmesvr.data_drive
          - sls: acmesvr.packages
acmesysdir:
  file.directory:
    - name: /data/acmesys
    - user: ubuntu
    - group: ubuntu
    - dir_mode: 775
    - file_mode: 664
    - require:
        - sls: acmesvr.data_drive
        - sls: acmesvr.packages
        - pkg: acmesys

  cmd.run:
    # setting the acmesvr_version grain and going highstate does not
    # upgrade the dockers if setup_log exists
    - name: /bin/bash ./acmesyssetup.sh cloud all {{ lv_version }} {{ server_name}} 2>&1 | tee -a {{ setup_log }}
    - runas: ubuntu
    - cwd: /data/acmesys
    - shell: /bin/bash
    - creates:
        - {{ setup_log }}
    - require:
        - sls: acmesvr.data_drive
        - sls: acmesvr.packages
        - file: {{ acme_log_dir }}
        - pkg: acmesys

