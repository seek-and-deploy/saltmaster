variable "account_id"               {                           }
variable "route53_tld"              {                           }
variable "public_zone_id"           {                           }
variable "tmpdir"                   {                           }
variable "tf_state_bucket"          {                           }
variable "project_config"           {                           }
variable "project"                  {                           }
variable "tf_state"                 {                           }
variable "ec2_ami"                  { default = "ami-b21507cd"  }
variable "ec2_instance_type"        { default = "t2.micro"      }
variable "salt_version"             { default = ""              }

