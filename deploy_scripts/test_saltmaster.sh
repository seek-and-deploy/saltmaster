#!/bin/sh -ex


echo "Checking: ${SM_HOST}"

./deploy_scripts/wait_for_salt.sh

echo "Configuring salt minion"

DOMAIN=$(echo ${SM_HOST} | cut -f2 -d.)

echo "Saltmaster: ${SM_HOST}"
echo "Domain: ${DOMAIN}"

mkdir -p /etc/salt/pki/minion

cat <<EOF > /etc/salt/minion
master: ${SM_HOST}
saltenv: mock
EOF

cat <<EOF > /etc/salt/minion_id
mockminion-c1-g1.${DOMAIN}
EOF
echo "Configured master as:"
cat /etc/salt/minion

echo "Configured minion as:"
cat /etc/salt/minion_id

echo "Running Salt"
salt-call -l debug state.apply

mock_dir=/tmp/mock_dir
mock_file="${mock_dir}/mock_file"

if [ -e "$mock_dir" ]
then
    echo "Found $mock_dir"
else
    exit 1
fi

if [ -d "$mock_dir" ]
then
    echo "$mock_dir is a directory!"
else
    exit 1
fi

if [ -f "$mock_file" ]
then
    echo "Found ${mock_file}"
    echo "$mock_file contents:"
    cat $mock_file
else
    exit 1
fi

exit 0
