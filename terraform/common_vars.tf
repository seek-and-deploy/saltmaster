######################################################################
# AWS Variables
###
variable "aws_key_name"       {                       }
variable "aws_region"         { default = "us-east-1" }

######################################################################
# Common config settings
###
variable "aws_profile"        {                 }
variable "env_name"           {                 }
variable "tags"               { type    = "map" }
variable "environment"        { type    = "map" }
variable "elb_map"            { type    = "map" }
variable "asg_map"            { type    = "map" }
variable "saltmaster_map"     { type    = "map" }
variable "ec2_map"            { type    = "map" }
variable "eip_allocation_id"  {                 }
