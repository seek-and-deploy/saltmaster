# -*- terraform -*-
resource "aws_route53_record" "saltmaster_pub_cname_record_single" {
  count   = "${var.elb_map["use_elb"] == 0 && var.asg_map["use_asg"] == 0 ? 1 : 0}"
  depends_on = [ "aws_instance.saltmaster" ]
  zone_id = "${var.public_dns_domain_id}"
  name    = "${var.pub_dns_name}"
  type    = "A"
  ttl     = 30
  records = ["${aws_instance.saltmaster.public_ip}"]
}

output "saltmaster_public_cname_single" {
  depends_on = [ "aws_route53_record.saltmaster_pub_cname_record_single" ]
  value = "${ aws_route53_record.saltmaster_pub_cname_record_single.0.fqdn } "
}

resource "aws_route53_record" "saltmaster_pri_cname_record_single" {
  count   = "${var.elb_map["use_elb"] || var.asg_map["use_asg"] ? 0 : 1}"
  zone_id = "${var.private_dns_domain_id}"
  name    = "${var.pri_dns_name}"
  type    = "A"
  ttl     = 30
  records = ["${aws_instance.saltmaster.private_ip}"]
}

output "saltmaster_private_cname_single" {
  value = "${aws_route53_record.saltmaster_pri_cname_record_single.*.fqdn}"
}

