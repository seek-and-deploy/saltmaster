#!/bin/bash

# Stop the registration thing
sudo systemctl stop acme
sleep 5
# stop aes
docker stop aes.acmesys
sleep 5
# get new aes
docker pull quay.io/acmesys/aes:d2-18-29-3-0209
sleep 1
docker run -d  --name aestest.acmesys -e TEST_MODE='10' --privileged quay.io/acmesys/aes:d2-18-29-3-0209
sleep 5
# Show the audio bar
docker logs -f aestest.acmesys &
sleep 30
docker stop aestest.acmesys
sleep 5
docker rm aestest.acmesys
sleep 5
docker start aes.acmesys
echo "end of AES test"
