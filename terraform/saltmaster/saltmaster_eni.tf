######################################################################
# ENI Creation
##
resource "aws_network_interface" "saltmaster" {
  subnet_id	  = "${ element( split(",", var.pub_subnet_ids),0)}"
  security_groups = [ "${split(",", var.sg_ids)}",
                     "${aws_security_group.salt_sg.id}"]
  private_ips       = [ "${cidrhost(element(split(",",var.pub_subnet_cidrs) , 0), 0 + 10)}" ]
  source_dest_check = false
  tags = "${merge(var.tags,
                  map("Name", "${var.tags["Name"]}"),
                  map("env",  "${ element( split("-", var.env_name),0) }"))}"
}

output "saltmaster_eni_ids" {
  value = "${aws_network_interface.saltmaster.id}"
}
output "saltmaster_eni_ips" {
  value = "${aws_network_interface.saltmaster.0.private_ips}"
}
