/tmp/mock_dir:
  file.directory:
    - mode: 755
    - makedirs: True
    - user: root
    - group: root

/tmp/mock_dir/mock_file:
  file.managed:
    - require:
        - file: /tmp/mock_dir
    - create: True
    - user: root
    - group: root
    - contents:
        - This is a mock file.
        - It contains mock data
