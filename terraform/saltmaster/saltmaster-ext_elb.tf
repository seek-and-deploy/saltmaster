# -*- terraform -*-
# Create an ELB load balancer to sit in front
resource "aws_elb" "saltmaster-ext" {
  count                     = "${var.elb_map["use_elb"] ? 1 : 0}"
  name                      = "${var.tags["system_type"]}-ext-${var.env_name}"
  internal                  = false
  cross_zone_load_balancing = true
  security_groups           = [ "${aws_security_group.salt_sg.id}",
                                "${aws_security_group.saltmaster_elb_ext.id}"
                              ]
  subnets = ["${split(",", var.pub_subnet_ids)}"]

  listener {
    lb_port               = 4505
    lb_protocol           = "tcp"
    instance_port         = 4505
    instance_protocol     = "tcp"
  }

  listener {
    lb_port               = 4506
    lb_protocol           = "tcp"
    instance_port         = 4506
    instance_protocol     = "tcp"
  }

  health_check {
    healthy_threshold     = "${var.elb_map["elb_healthy_threshold"]}" 
    unhealthy_threshold   = "${var.elb_map["elb_unhealthy_threshold"]}"
    timeout               = "${var.elb_map["elb_timeout"]}"
    interval              = "${var.elb_map["elb_interval"]}"
    target                = "${var.elb_map["elb_health-check"]}"
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = "${merge(var.tags,
                  map("Name", "${var.tags["system_type"]}-ext-${var.env_name}"),
                  map("env",  "${var.env_name}"))}"
  
}

output "saltmaster_elb_ext_id" {
  value = "${aws_elb.saltmaster-ext.*.id}"
}
output "saltmaster_elb_ext_name" {
  value = "${aws_elb.saltmaster-ext.*.name}"
}
output "saltmaster_elb_ext_dns_name" {
  value = "${aws_elb.saltmaster-ext.*.dns_name}"
}
output "saltmaster_elb_ext_instances" {
  value = "${aws_elb.saltmaster-ext.*.instances}"
}
output "saltmaster_elb_ext_source_security_group" {
  value = "${aws_elb.saltmaster-ext.*.source_security_group}"
}
output "saltmaster_elb_ext_source_security_group_id" {
  value = "${aws_elb.saltmaster-ext.*.source_security_group_id}"
}
output "saltmaster_elb_ext_zone_id" {
  value = "${aws_elb.saltmaster-ext.*.zone_id}"
}
