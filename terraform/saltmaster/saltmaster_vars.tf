# -*- terraform -*-
######################################################################
# Passed in variables.
###
variable "aws_account_id"        { }
variable "allowed_cidr_blocks"   { }
variable "project_config"        { }
variable "pri_subnet_ids"        { }
variable "pub_subnet_ids"        { }
variable "pub_subnet_cidrs"      { }
variable "pri_subnet_cidrs"      { }

variable "sg_ids"                { }
variable "tmpdir"                { }
variable "vpc_id"                { }
variable "pub_dns_name"          { }
variable "pri_dns_name"          { }
variable "pub_dns_domain"        { }
variable "pri_dns_domain"        { }
variable "public_dns_domain_id"  { }
variable "private_dns_domain_id" { }
variable "ec2_ami"               { default = "ami-e552b2f3"            }
variable "ec2_instance_type"     { default = "m3.medium"               }

######################################################################
# Local vars
##
variable "instance_profile"      { default = ""                        }
variable "instance_role_id"      { default = ""                        }

######################################################################
# Module Locals
##
variable "create_s3_buckets"  { default = 0 }
locals {
  allowed_cidr_blocks = ["${ split(",", var.elb_map["use_elb"] ? var.allowed_cidr_blocks : "0.0.0.0/0") }" ]
  pub_fqdn = "${var.pub_dns_name}.${var.pub_dns_domain}"
  pri_fqdn = "${var.pri_dns_name}.${var.pri_dns_domain}"
}

