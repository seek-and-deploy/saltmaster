# -*- terraform -*-
######################################################################
# Saltmaster host
##
resource "aws_instance" "saltmaster" {
  count                       = "${var.elb_map["use_elb"] || var.asg_map["use_asg"] ? 0 : 1}"
  ami                         = "${var.ec2_ami}"
  instance_type               = "${var.ec2_map["instance_type"]}"

  key_name                    = "${var.aws_key_name}"
  associate_public_ip_address = true
  subnet_id                   = "${ element( split(",", var.pub_subnet_ids),0)}"

  vpc_security_group_ids      = [ "${split(",", var.sg_ids)}",
                                  "${aws_security_group.salt_sg.id}"]
  iam_instance_profile = "${var.instance_profile}"
  user_data	       = "${data.template_file.saltmaster_userdata.rendered}"

  root_block_device = {
    volume_type = "${var.ec2_map["root_volume_type"]}"
    volume_size = "${var.ec2_map["root_volume_size"]}"
    delete_on_termination = true
  }
    
  tags		       = "${merge(var.tags,
                  	    map("Name", "${var.tags["Name"]}"),
                  	    map("env",  "${ element( split("-", var.env_name),0) }"))}"
}

output "saltmaster_id" {
   value = "${aws_instance.saltmaster.*.id}"
}
output "saltmaster_public_ip" {
   value = "${aws_instance.saltmaster.*.public_ip}"
}
output "saltmaster_private_ip" {
   value = "${aws_instance.saltmaster.*.private_ip}"
}

