Generic ACME Saltmaster configuration for self-bootstrapping serverless minion.

The code for the saltmaster is split into two main trees:

- acme-saltmaster-1.0: where the actual salt configuration files go.
- terraform: where the terraform code goes.

The acme-saltmaster-1.0 hierarchy has two main areas of code:

- debian/ - Where all the code for building and managing a debian .deb package is located
- all other directories and files that will be installed via the .deb package relative to /

The `terraform/` directory is structured in the form of a `terraform` module and is intended to be deployed using the terraform-deployer: https://github.com/veracode/terraform-deployer


To build the Debian package:

`$ make build`

To build a basic debian repository locally for testing:

`$ make repo`
