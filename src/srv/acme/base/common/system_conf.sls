/etc/apt/apt.conf.d/10periodic:
  file.managed:
    - source: salt://common/files/10periodic
    - user: root
    - user: root
    - mode: 644

{% from 'common/map.jinja' import common with context -%}
domain_name:
  grains.present:
    - value: {{ common.domain_name }}
    - force: True

public_fqdn:
  grains.present:
    - value: {{ common.public_fqdn }}
    - force: True

env_name:
  grains.present:
    - value: {{ common.env_name }}
    - force: True

env_version:
  grains.present:
    - value: {{ common.env_version }}
    - force: True

system_type:
  grains.present:
    - value: {{ common.system_type }}
    - force: True
