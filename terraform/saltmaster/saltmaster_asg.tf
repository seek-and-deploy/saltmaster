# -*- terraform -*-
resource "aws_autoscaling_group" "saltmaster" {
  count                     = "${var.asg_map["use_asg"] ? 1 : 0}"
  name                      = "${aws_launch_configuration.saltmaster.name}"
  launch_configuration      = "${aws_launch_configuration.saltmaster.name}"
  vpc_zone_identifier       =  ["${ element( split(",", var.pub_subnet_ids),0)}"]
  min_size                  = "${var.asg_map["asg_min_size"]}"
  max_size                  = "${var.asg_map["asg_max_size"]}"
  desired_capacity          = "${var.asg_map["asg_desired_capacity"]}"
  wait_for_capacity_timeout = "${var.asg_map["asg_capacity_timeout"]}"

  lifecycle {
    create_before_destroy = true
  }

  # This ugliness is a result of Terraform being too dumb to convert a
  # straight hash into a list of hashes necessary for use with ASGs.
  # Therefore, here we tag our instances at launch with only the most
  # required tags, then let the instance self-tag in the launch config
  # with whatever other tags we may want.
  tags = [
    {
      key                 = "Name"
      value               = "${var.tags["Name"]}"
      propagate_at_launch = true
    },
    {
      key                 = "env"
      value               = "${var.env_name}"
      propagate_at_launch = true
    },
    {
      key                 = "owner"
      value               = "${var.tags["owner"]}"
      propagate_at_launch = true
    },
    {
      key                 = "email"
      value               = "${var.tags["email"]}"
      propagate_at_launch = true
    },
    {
      key                 = "group"
      value               = "${var.tags["group"]}"
      propagate_at_launch = true
    },
    {
      key		  = "product"
      value		  = "${var.tags["product"]}"
      propagate_at_launch = true
    },
    {
      key		  = "system_type"
      value		  = "${var.tags["system_type"]}"
      propagate_at_launch = true
    },
  ]

}
